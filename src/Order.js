import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';


class Order extends Component {
    constructor(props) {
        super(props)
        this.state = {
            value: '',
            redirect: false
        }

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick = () => {
        fetch('http://localhost:8080/orders')
            .then(response => response.json())
            .then((data) => {
                this.setState({
                    value: JSON.stringify(data)
                })
            })
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/welcome' />
        }
    }

    redirectToTarget = () => {
        this.setState({
            redirect: true
        })
    }

    render() {
        return (
            <>
                {this.renderRedirect()}
                <table>
                    <thead>
                        <tr>
                            <th>Item ID</th>
                            <th>Item Name</th>
                            <th>Item Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody >
                </table >
                <p>{this.state.value} </p>
                <button onClick={this.handleClick}>Get items</button>
                <button onClick={this.redirectToTarget}>Back</button>
            </>
        );
    }
}

export default Order;