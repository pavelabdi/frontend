import React from 'react';
const uuidv1 = require('uuid/v1')

class Items extends React.Component {

    render() {
        return (
            Object.keys(this.props.arr).map((el) => {
                return (
                    <li key={uuidv1()}>{this.props.arr[el]}</li>
                )
            })
        )
    }
}

export default Items