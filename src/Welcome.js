import React from "react";
import Items from './Items';
import { Redirect } from 'react-router-dom';

class Welcome extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            value: '',
            authuser: '',
            items: [],
            selecteditems: {},
            selecteditem: 'Cup',
            redirect: false
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount() {
        let initialItems = [];
        fetch('http://localhost:8080/welcome')
            .then(response => response.json())
            .then(data => {
                initialItems = data.map((item) => {
                    return item
                });
                console.log(initialItems);
                this.setState({
                    items: initialItems,
                });
            });
    }

    handleChange(e) {
        this.setState({ selecteditem: e.target.value });
        console.log(this.state.selecteditem)
    }

    handleSubmit() {
        this.setState(
            (prevState) => {
                let newObj = { ...prevState.selecteditems }
                newObj[`${Date.now()}`] = prevState.selecteditem
                return {
                    selecteditems: newObj
                }
            }
        )
        console.log(this.state.selecteditems)
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/orders' />
        }
    }

    render() {
        let items = this.state.items;
        let optionItems = items.map((item) =>
            <option key={item.name}>{item.name}</option>
        );
        return (
            <>
                {this.renderRedirect()}
                <p>{this.state.value} </p>
                <button onClick={this.handleClick}>Get items</button>
                <h2>Hello</h2>
                <h3>make choice</h3>
                Product:
                <select onChange={this.handleChange}>
                    {optionItems}
                </select>
                <button onClick={this.handleSubmit}>Add</button>
                <button onClick={this.redirectToTarget}>Order</button>

                <p>Selected items</p>
                <Items arr={this.state.selecteditems} />
            </>
        );
    }

    redirectToTarget = () => {
        var arr = [];
        for (var key in this.state.selecteditems) {
            arr.push(this.state.selecteditems[key]);
        }
        fetch('http://localhost:8080/orders', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                items: arr
            })
        });
        this.setState({
            redirect: true
        });
        console.log(arr)
    }

    handleClick = () => {
        fetch('http://localhost:8080/welcome')
            .then(response => response.json())
            .then((data) => {
                this.setState({
                    value: JSON.stringify(data)
                })
            })
    }
}

export default Welcome;