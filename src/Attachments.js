import React from "react";

class Attachments extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          file : null,
          pic : null
        }
        this.onFormSubmit = this.onFormSubmit.bind(this)
        this.onChange = this.onChange.bind(this)
        this.fileUpload = this.fileUpload.bind(this)
        this.handleClick = this.handleClick.bind(this)
      }

    onFormSubmit(e) {
        e.preventDefault() // Stop form submit
        this.fileUpload(this.state.file).then((response) => {
            console.log(response.data);
        })
    }

    onChange(e) {
        this.setState({ 
            file: e.target.files[0] 
        })
    }

    fileUpload(file) {
        const url = 'http://localhost:8080/attachments';
        const formData = new FormData();
        formData.append('file', file)
        return fetch(url, {
            method: 'POST',
            body: formData
            }
        )       
    }

    render() {
        return (
            <>
                <p>{this.state.value} </p>
                <img src = {this.state.pic} alt = ''></img>
                <form onSubmit={this.onFormSubmit}>
                    <h1>File Upload</h1>
                    <input type="file" onChange={this.onChange} />
                    <button type="submit">Upload</button>
                </form>
                <button onClick={this.handleClick}>Get attachment</button>
            </>
        );
    }

    handleClick = () => {
        const { params } = this.props.match
        let request = ''
        if (params.id === undefined){
            request = 'http://localhost:8080/attachments'
        }else {
            request = `http://localhost:8080/attachments/${params.id}`
        }
        this.setState({
            pic : request
        })        
    }
}

export default Attachments;