import React from "react"

class Users extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          value: '',
          myList: {}
        }
      }
    
    render() {
        return (
            <>
            <p>{this.state.value} </p>
            <button onClick={this.handleClick}>Send an HTTP GET request and get the result back</button>
            </>
        );
    }

    handleClick = () => {
        const { params } = this.props.match
        let request = ''
        if (params.id === undefined){
            request = 'http://localhost:8080/users'
        }else {
            request = `http://localhost:8080/users/${params.id}`
        }
        fetch(request)
            .then(response => response.json())
            .then((data) => {
                this.setState({
                    value: JSON.stringify(data)
                })
            })   
    }
}

export default Users;