import React, { Component } from 'react'
import './index.css'
import Form from './Form'

const inputs = [{
  name: "username",
  placeholder: "username",
  type: "text"
},{
  name: "password",
  placeholder: "password",
  type: "password"
},{
  type: "submit",
  value: "Submit",
  className: "btn"
}]
 
const props = {
  name: 'loginForm',
  method: 'POST',
  action: 'http://localhost:8080/doLogin',
  inputs: inputs
}
 
const params = new URLSearchParams(window.location.search)
console.log(params)

// fetch('http://localhost:8080/users')
//             .then(response => response.json())
//             .then((data) => {
//                 this.setState({
//                     value: JSON.stringify(data)
//                 })
//             })  

class Login extends Component {

    redirectToTarget = () => {
        this.props.history.push(`/user-registration`)
      }

    render(){
        return (
            <>
            <Form {...props} error={params.get('error')} />
            <button onClick={this.redirectToTarget}>Sign up</button>
            </>
        )
    }
}

export default Login