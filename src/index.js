import React from 'react';
import ReactDOM from 'react-dom';
import {
    BrowserRouter,
    HashRouter,
    Route,
    Link
  } from 'react-router-dom';
import './index.css';
import Users from './Users';
import UserRegistration from './UserRegistration';
import Login from './Login';
import Welcome from './Welcome';
import Order from './Order';
import Attachments from './Attachments';

ReactDOM.render(
    <BrowserRouter>
        <Route exact path="/" component={Login}></Route>
        <Route exact path="/users" component={Users}></Route>
        <Route exact path="/attachments" component={Attachments}></Route>
        <Route path="/user-registration" component={UserRegistration}></Route>
        <Route path="/login" component={Login}></Route>
        <Route path="/welcome" component={Welcome}></Route>
        <Route path="/orders" component={Order}></Route>
        <Route path="/orders/:id" component={Order}></Route>
        <Route path="/users/:id" component={Users}></Route>
        <Route path="/attachments/:id" component={Attachments}></Route>
    </BrowserRouter>, 
document.getElementById('root'));