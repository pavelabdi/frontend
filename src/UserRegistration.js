import React, {Component} from 'react';
import { Redirect } from 'react-router-dom';

class UserRegistration extends Component {
    constructor(props){
        super(props);
        this.state={
            name:"",
            password:"",
            redirect: false
        };

    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeName(event){
        event.preventDefault();
        this.setState({name: event.target.value});
    }

    handleChangePassword(event){
        event.preventDefault();
        this.setState({password: event.target.value});
    }

    handleSubmit(event){
        event.preventDefault();

        fetch('http://localhost:8080/users', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: this.state.name,
                password: this.state.password,
                role: 'USER'
            })
        });
        this.setState({
            redirect: true
        })
    }

    renderRedirect = () => {
        if (this.state.redirect){
            return <Redirect to='/login' />
        }
    }

    render(){
        return(
            <>
            {this.renderRedirect()}
            <form onSubmit={this.handleSubmit}>
                <label>User Name: 
                <input type="text" name="name" value={this.state.name} onChange={this.handleChangeName} />
                </label>
                <label>Password: 
                <input type="password" name="password" value={this.state.password} onChange={this.handleChangePassword} />
                </label>
                <input type="submit" value="Register"/>
            </form>
            </>
        );  
    }
}

export default UserRegistration;